#ifndef PAYMENT_H
#define PAYMENT_H

#include <QWidget>
#include <QDate>
#include <QtSql/QSqlDatabase>
#include <QPushButton>
#include <QDateEdit>
#include <QLineEdit>
#include <QLinkedList>
class Payment : public QWidget
{
    Q_OBJECT
public:
    explicit Payment(QWidget *parent = 0,QSqlDatabase* db=new QSqlDatabase(),int id_patient=0);
    ~Payment();
    int getSolde() const;
    void updateSolde();

    int getDevis() const;
    void updateDevis();

private:
    QSqlDatabase data;
    QWidget* container;
    QPushButton* addButton;
    QDateEdit* date;
    QLineEdit* text;
    QPushButton* addPay;
    QLinkedList<QWidget*> rowsList;
    int devis;
    int solde;
    int id_pat;
    void afficherPayment();
signals:
    void paymentUpdate();
public slots:
    void supprimerPayment();
    void ajouterPayment();
};

#endif // PAYMENT_H
