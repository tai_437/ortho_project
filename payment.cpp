#include "payment.h"
#include <QBoxLayout>
#include <QLabel>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QPushButton>
#include <QObjectUserData>
//constructeur
Payment::Payment(QWidget *parent,QSqlDatabase* db,int id_patient) : QWidget(parent)
{
    if(db->open()){
        data=*db;
    }
    this->container=new QWidget();
    this->addButton=new QPushButton("+");
    this->date=new QDateEdit(QDate::currentDate());
    this->text=new QLineEdit();
    this->text->setValidator(new QIntValidator(0,10000));
    this->addPay=new QPushButton("ajouter");
    this->id_pat=id_patient;
    this->setWindowModality(Qt::ApplicationModal);
    this->updateDevis();
    this->updateSolde();
    this->setLayout(new QVBoxLayout());
    this->layout()->addWidget(container);
    connect(this->addButton,SIGNAL(clicked()),this,SLOT(ajouterPayment()));
    connect(this->addPay,SIGNAL(clicked()),this,SLOT(ajouterPayment()));
    Payment::afficherPayment();
}
//FIN constructeur

void Payment::afficherPayment(){
    QSqlQuery query(this->data);
    query.prepare("select * from payment where id_patient=:id");
    query.bindValue(":id",this->id_pat);
    if(query.exec()){
    QVBoxLayout* lay=new QVBoxLayout();
    while(!this->rowsList.isEmpty())
    delete this->rowsList.takeFirst();
    this->rowsList.clear();
    while(query.next()){
        QWidget* w=new QWidget();
        w->setLayout(new QHBoxLayout());
        w->layout()->addWidget(new QLabel(query.value("date_payment").toDate().toString("dd MMMM yyyy")));
        w->layout()->addWidget(new QLabel(query.value("montant").toString()+"DA"));
        QPushButton* btn=new QPushButton("supprimer");
        btn->setProperty("id_payment",QVariant(query.value("id_payment").toInt()));
        connect(btn,SIGNAL(clicked()),this,SLOT(supprimerPayment()));
        w->layout()->addWidget(btn);
        lay->addWidget(w);
        this->rowsList << w;
    }
    QWidget* addw=new QWidget();
    addw->setLayout(new QHBoxLayout());
    addw->layout()->addWidget(this->date);
    addw->layout()->addWidget(this->text);
    addw->layout()->addWidget(this->addPay);
    lay->addWidget(addw);
    delete container;

    container=new QWidget();

    container->setLayout(lay);
    this->layout()->addWidget(container);
    }else{
        qDebug() << "erreur afficher_payment_sql"+query.lastError().text();
    }
}

void Payment::supprimerPayment(){
    QPushButton* s=(QPushButton*)QObject::sender();
    int id=s->property("id_payment").toInt();
    QSqlQuery query(this->data);
    query.prepare("delete from payment where id_payment=:id");
    query.bindValue(":id",id);
    if(!query.exec())
    qDebug() << "error supprimer_Payment: "+query.lastError().text();
    updateSolde();
    updateDevis();
    Payment::afficherPayment();
    container->updateGeometry();
    this->updateGeometry();
    emit paymentUpdate();
}

void Payment::ajouterPayment(){
    QSqlQuery query(this->data);
    if(this->text->text().isEmpty())
        return;
    query.prepare("insert into payment(id_patient,montant,date_payment) values(:id_patient,:montant,:date_payment)");
    query.bindValue(":id_patient",QVariant(this->id_pat));
    query.bindValue(":montant",QVariant(this->text->text()));
    query.bindValue(":date_payment",this->date->date().toString("yyyy-M-d"));
    if(!query.exec()){
        qDebug() << "\t error ajouter_payment:";
        qDebug() << query.lastQuery();
        qDebug() << query.lastError().text();
    }
    updateDevis();
    updateSolde();
    Payment::afficherPayment();
    emit paymentUpdate();
}

Payment::~Payment()
{

}
int Payment::getSolde() const
{
    return this->solde;
}
void Payment::updateSolde()
{
    QSqlQuery g_solde(this->data);
    g_solde.prepare("select SUM(montant) as solde from payment where id_patient=:id");
    g_solde.bindValue(":id",this->id_pat);
    if(g_solde.exec()){
        g_solde.next();
        this->solde=g_solde.value("solde").toInt();
    }else{
        qDebug() << "erreur get_solde: "+this->id_pat;
    }
}
int Payment::getDevis() const
{
    return this->devis;
}
void Payment::updateDevis()
{
    QSqlQuery g_devis(this->data);
    g_devis.prepare("select devis from patient where id_patient=:id");
    g_devis.bindValue(":id",this->id_pat);
    if(g_devis.exec()){
        g_devis.next();
        this->devis=g_devis.value("devis").toInt();
    }else{
        qDebug() << "erreur get_devis: "+g_devis.lastError().text();
    }
}


