#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T19:11:59
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ortho_project
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    payment.cpp \
    patient.cpp

HEADERS  += mainwindow.h \
    payment.h \
    patient.h
