#ifndef PATIENT_H
#define PATIENT_H

#include <QWidget>
#include <QSqlQuery>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <payment.h>
#include <QList>
#include <QFormLayout>
#include <QScrollArea>
class Patient : public QWidget
{
    Q_OBJECT
public:
    explicit Patient(QWidget *parent = 0,QSqlDatabase* db=new QSqlDatabase(), int id_patient=0);
    explicit Patient(QWidget *parent = 0, QSqlDatabase* db=new QSqlDatabase());
    ~Patient();
private:
    QSqlDatabase data;
    int id_pat;
    QLabel* profilPic;
    QLineEdit* nom;
    QLineEdit* prenom;
    QLineEdit* age;
    QLineEdit* corres;
    QLineEdit* ante;
    QLineEdit* diagno;
    QFormLayout* form;
    QLabel* devis;
    QLabel* solde;//montant restant a payer
    Payment* pay;
    QPushButton* det_btn;
    QPushButton* radio_btn;
    QPushButton* photo_btn;
    QPushButton* save_btn;
    QWidget* traitement_container;
    QList<QList<QWidget*> > rowList;
    QScrollArea* temparea;
    QLineEdit* trait_text;
    QPushButton* trait_add;
    QDateEdit* trait_date;
    void initGui();
    void initEmptyGui();
signals:

public slots:
    void saveChanges();
    void updateData();
    void updateTraitement();
    void update_devis_n_Solde();
    void deleteTraitement();
    void enable_save();
    void ajouterTraitement();

};

#endif // PATIENT_H
