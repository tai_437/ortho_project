#include "patient.h"
#include <QVBoxLayout>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QDebug>
#include <QPixmap>
#include <QSqlError>
#include <QScreen>
#include <QApplication>
#include <QDesktopWidget>
Patient::Patient(QWidget *parent,QSqlDatabase* db,int id_patient) : QWidget(parent)
{
    this->data=*db;
    this->id_pat=id_patient;
    initGui();
    if(id_patient>0)
        updateData();
    this->save_btn->setEnabled(false);
}
void Patient::initGui(){
    this->setMinimumSize(QSize(600,600));
    profilPic=new QLabel();
    nom=new QLineEdit();
    prenom=new QLineEdit();
    age=new QLineEdit();
    corres=new QLineEdit();
    ante=new QLineEdit();
    diagno=new QLineEdit();
    pay=new Payment(0,&data,this->id_pat);
    devis=new QLabel("devis:"+QString::number(this->pay->getDevis()));
    solde=new QLabel("solde:"+QString::number(this->pay->getSolde()));
    det_btn=new QPushButton("detail sur payment");
    radio_btn=new QPushButton("Radios");
    photo_btn=new QPushButton("Photos");
    save_btn=new QPushButton("SAUVGARDER");
        save_btn->setEnabled(false);
    traitement_container=new QWidget();
    temparea=new QScrollArea();
    trait_add=new QPushButton("Ajouter");
    trait_text=new QLineEdit();
    trait_date=new QDateEdit(QDate::currentDate());

    QHBoxLayout* globalLay=new QHBoxLayout();
        this->setLayout(globalLay);
    QVBoxLayout* profilLay=new QVBoxLayout();
        profilLay->addWidget(profilPic);
        profilLay->addWidget(photo_btn);
        profilLay->addWidget(radio_btn);
        profilLay->insertStretch(3);
        profilLay->addWidget(save_btn);
        profilLay->insertStretch(5);
        profilLay->addWidget(devis);
        profilLay->addWidget(solde);
        profilLay->addWidget(det_btn);
        this->profilPic->setPixmap(QPixmap("C:/SSA/Qt/anr.png"));

        form=new QFormLayout();
        form->addRow("nom",this->nom);
        form->addRow("prenom",this->prenom);
        form->addRow("age",this->age);
        form->addRow("correspondant",this->corres);
        form->addRow("antecedant",this->ante);
        form->addRow("diagnostique",this->diagno);
        form->addRow(this->temparea);//################
            QHBoxLayout* lay=new QHBoxLayout();
            lay->addWidget(trait_date);
            lay->addWidget(trait_text);
            lay->addWidget(trait_add);
            QWidget* t=new QWidget();
            t->setLayout(lay);
            form->addRow(t);

        globalLay->addLayout(form);
        globalLay->addLayout(profilLay);
        connect(this->det_btn,SIGNAL(clicked()),this->pay,SLOT(show()));
        connect(this->pay,SIGNAL(paymentUpdate()),this,SLOT(update_devis_n_Solde()));
        connect(this->save_btn,SIGNAL(clicked()),SLOT(saveChanges()));

        connect(nom,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
        connect(prenom,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
        connect(age,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
        connect(corres,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
        connect(ante,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
        connect(diagno,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
        connect(trait_add,SIGNAL(clicked()),this,SLOT(ajouterTraitement()));

}

void Patient::updateData(){
    QSqlQuery* sql=new QSqlQuery();
    sql->prepare("select * from patient where id_patient=:id");
    sql->bindValue(":id",QVariant(this->id_pat));
    if(sql->exec()){
        sql->next();
        this->nom->setText(sql->value("nom").toString());
        this->prenom->setText(sql->value("prenom").toString());
        this->age->setText(sql->value("age").toString());
        this->corres->setText(sql->value("correspondant").toString());
        this->ante->setText(sql->value("antecedant").toString());
        this->diagno->setText(sql->value("diagnostique").toString());
        QPixmap p("C:/SSA/Qt/anr.png");
        this->profilPic->setPixmap(p);
    }else{
        qDebug() << "error update_Data: "+sql->lastError().text();
    }
    updateTraitement();
}

void Patient::updateTraitement(){
    QSqlQuery*sql=new QSqlQuery(this->data);
    sql->prepare("select * from traitement where id_patient=:id");
    sql->bindValue(":id",QVariant(this->id_pat));
    if(sql->exec()){
        int i=0;
        for(i=0;i<rowList.length();i++){
            disconnect(rowList[i][1],SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
            disconnect(rowList[i][2],SIGNAL(clicked()),this,SLOT(deleteTraitement()));
            delete rowList[i][0];
            delete rowList[i][1];
            delete rowList[i][2];
        }
        rowList.clear();
        delete traitement_container;
        traitement_container=new QWidget();
        QFormLayout* formulaire=new QFormLayout();
        while(sql->next()){
            i++;
            QList<QWidget*> tab;
            QLabel* lab=new QLabel(sql->value("date").toDate().toString("dd MMMM"));
            QLineEdit* edit=new QLineEdit(sql->value("desc").toString());
            QPushButton* btn=new QPushButton("SUPP");
            tab.push_back(lab);
            tab.push_back(edit);
            tab.push_back(btn);
            rowList.push_back(tab);

            btn->setProperty("id_traitement",QVariant(sql->value("id_traitement").toInt()));
            QHBoxLayout* templay=new QHBoxLayout();
            templay->addWidget(edit,9);
            templay->addWidget(btn,1);
            formulaire->addRow(sql->value("date").toDate().toString("dd MMMM"),templay);
            connect(edit,SIGNAL(textChanged(QString)),this,SLOT(enable_save()));
            connect(btn,SIGNAL(clicked()),this,SLOT(deleteTraitement()));
        }
        traitement_container->setLayout(formulaire);
        this->temparea->setWidget(traitement_container);

    }else{
        qDebug() << "error update_traitement: "+sql->lastError().text();
    }
    temparea->updateGeometry();
}

void Patient::update_devis_n_Solde(){
    this->devis->setText(QString("Devis: ")+QString::number(this->pay->getDevis()));
    this->solde->setText(QString("Solde: ")+QString::number(this->pay->getSolde()));
}

void Patient::enable_save(){
    this->save_btn->setEnabled(true);
}

void Patient::deleteTraitement(){
    qDebug() << "delete traitement:" ;
    QPushButton* sndr=(QPushButton*)sender();

    QSqlQuery sql(this->data);
    sql.prepare("delete from traitement where id_traitement=:id");
    sql.bindValue(":id",sndr->property("id_traitement"));
    if(sql.exec()){
        updateTraitement();
    }else{
        qDebug() << "erreur delete traitement" << sql.lastError().text();
    }
}

void Patient::ajouterTraitement(){
    QSqlQuery sql(this->data);
    sql.prepare("insert into traitement(id_patient,desc,date) values(:id,:desc,:date)");
    sql.bindValue(":id",this->id_pat);
    sql.bindValue(":desc",this->trait_text->text());
    sql.bindValue(":date",this->trait_date->date().toString("yyyy-M-d"));
    if(sql.exec()){
        updateTraitement();
    }else{
        qDebug() << "erreur ajouter_traitement" << sql.lastError().text();
    }
}


void Patient::saveChanges(){
    qDebug() << "saveChanges!!";
    QSqlQuery sql(this->data);
    sql.prepare("update patient set nom=:nom,prenom=:prenom,age=:age,correspondant=:corres,antecedant=:ante,diagnostique=:diagno where id_patient=:id");
    sql.bindValue(":nom",QVariant(nom->text()));
    sql.bindValue(":prenom",QVariant(prenom->text()));
    sql.bindValue(":age",QVariant(age->text()));
    sql.bindValue(":corres",QVariant(corres->text()));
    sql.bindValue(":ante",QVariant(ante->text()));
    sql.bindValue(":diagno",QVariant(diagno->text()));
    sql.bindValue(":id",QVariant(this->id_pat));
    if(sql.exec()){
        updateData();
    }else{
        qDebug() << "erreur save changes: " << sql.lastError().text() << "--" << sql.lastQuery();
    }
    save_btn->setEnabled(false);
}
Patient::~Patient(){

}
